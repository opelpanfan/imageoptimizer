@echo none
SET mypath=%~dp0

REM Optimizing JPEG with jpegtran
forfiles /p %1 /s /m "*.jpg" /c "cmd /c echo processing @path && %mypath:~0,-1%\jpegtran.exe -optimize -progressive -copy none -outfile @path @path"
REM Optimizing PNG with pngout
forfiles /p %1 /s /m "*.png" /c "cmd /c echo processing @path && %mypath:~0,-1%\pngout.exe @path" 
REM Optimizing PNG with optipng
rem forfiles /p %1 /s /m "*.png" /c "cmd /c echo processing @path && %mypath:~0,-1%\optipng.exe -force -o7 @path"
pause