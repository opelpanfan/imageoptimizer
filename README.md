# ImageOptimizer#

### What is this repository for? ###

Optimize JPEG and PNG Images Using Google's recommended compression tools:

For JPG:
    Jpegtran 

For PNG:
OptPNG, PNGOUT

### How do I get set up? ###

   1. Extact ImageOptimizer.7zip
   2. Run optimize.bat {path to image location}

Script drills to every folder of the specified path and looks for .jpeg and.png files.